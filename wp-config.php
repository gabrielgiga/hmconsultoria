<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'hm' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'root' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', '' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'u;~K,rZ-L0Ms]k_hqD.XmmRLZ;Wi3%mA(E3WCwmx$kMQ+O9M=^r R$lp{Z7Ch-${' );
define( 'SECURE_AUTH_KEY',  'fokW/0_$S0Z]h1L^$C)R3Eg[y^_IEjdc{J1GxgCpHY-dX<H;o&a<K[a8hUe7^Bh?' );
define( 'LOGGED_IN_KEY',    '7(v&mm$bp{5@4B@sYy=;w+/T&SAaaKI@MB&i%Qg!%Z$ggfAQl@mN8b^>`Wxfpjq4' );
define( 'NONCE_KEY',        'CA4tQX@fB>P}$PK`ozi?doCLk4Jjt:aBO1+NU_QX5(X&*Rqmsr6+D?oJ|GJd!=0A' );
define( 'AUTH_SALT',        '1nM8F/lg8^Qs91z 5,C%AhZ+Lx})=^l4ratn>z?eJ[> d`n}ZKq(bEXUe?5xi<Vq' );
define( 'SECURE_AUTH_SALT', 'Kc_Ol4MpqVcJbd2I/|o/b|D11)do9U_$@m^Ajj>h#:[~{i-R*>]%3t_F.X*P`k!2' );
define( 'LOGGED_IN_SALT',   '3d.>klWHhx<^W>;Z`zT<KPJ$zDCT6pJL[#;9p}nPMLR-@$I>JR=~bK:2<{u.oNyw' );
define( 'NONCE_SALT',       'MTYOJ:|,cwa3&HT.e:)GV^vA@wD3lJ^^7_!8b[D1opj[wRgb.IbYm>pc,ns(LEcp' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
